#   Arropados Ltda.
De acuerdo a la necesidad de la empresa **Arropados Ltda.** se ha decidido el siguiente desarrollo en pro de mejorar la experiencia del usuario en el sitio web, implementando los últimos estándares de desarrollo web.

Análisis: Pensando en un sitio web para la exposición y ventas de productos de la empresa Arropados Ltda.  se tienen en cuenta los siguientes factores:



 **Tipo de diseño:** Debido a los requerimientos del cliente, optamos por utilizar un tipo de diseño Web Responsive Design “Diseño web responsivo” , lo que da la facilidad al sitio web de adaptarse a cualquier dispositivo en el que este se visualice, de manera que la usabilidad y facilidad de lectura de la información sea agradable para el usuario, será un sitio web dinámico que permita al administrador gestionar el contenido de publicación, tales como crear, modificar, o borrar un producto.


**Web Layout:**
- Versión Desktop

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/Desktop.png)](Desktop)

- Version Tablet

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/Tablet.png)](Tablet)

- Version Smartphone

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/Smartphone.png)](Imagen4)


**Tecnologías:** Se haría uso de las siguientes tecnologías:


| Tecnología | Uso |
| ------ | ------ |
| Jquery | Es una biblioteca para manejar más fácil los eventos, desarrollar animaciones y que el sitio web funcione de manera asíncrona, es decir hacer cambios en la página sin necesidad de que esta se recargue ya que para algunos usuarios, recargar la página implica demorarse más y esto puede ser molesto y el  objetivo es mantener a los usuarios a gusto con el sitio web. |
| Sass | Se usará sass para escribir de manera más rápida y funcional                archivos css. |
|Babel| Esta herramienta nos ayudará a transformar los archivos Javascript           con las nuevas características de ES6 a ES5, aprovechando así las            mejoras que ES6 nos ofrece. |
|Webpack|   Nos ayudará a crear un bundle en el que no necesitemos un transpilador para Sass y Babel, sino que solo nuestro bundle se cree con un solo transpilador.|

# Surrender20 Co
Teniendo en cuenta que el sitio web desarrollado actualmente para la empresa **Surrender20 Co** es de tipo **fluido**, lo cual no permite al usuario tener una buena interacción ya que el contenido es medido en porcentajes de acuerdo al tamaño de la ventana, lo que quiere decir que si la pantalla es muy grande como la de un escritorio, el contenido se vería demasiado grande y si se viera el sitio web desde un móvil, el contenido se vería muy pequeño, lo cual sería ilegible para el usuario y esto tendría un impacto negativo en la experiencia de usuario, teniendo en cuenta que el sitio web ofrece el servicio de asesoramiento en línea, por lo que el contenido que se muestra debe ser claro y verse bien.

Como solución al problema planteado anteriormente, se propone un tipo de diseño **adaptativo** en el cual se puede desarrollar plantillas definidas para cierto tipo de pantallas y es el modo de trabajo más funcional para clientes con un presupuesto reducido, creando así tres plantillas:
*   Desktop(Escritorio): plantilla con una resolución full HD de 1280px x 1024px.
*   Tablet(Tableta): plantilla con una resolución de 800px x 1280px.
*   Smartphone(Móvil): plantilla con una resolución de 750px x 1334 px.

Debido a que **Surrender20** Co requiere que su sitio web en versiones para smartphone y tablets sea más fluido y optimizada en términos de presentación de información, haciendo así el sitio web más rápido e intuitivo.

# Polyfill     
Un Polyfill es una pieza de código o plugin que permite a los desarrolladores web trabajar con una tecnología que no soporta el navegador de forma nativa, es decir permite usar características de navegadores nuevos en navegadores antiguos y así olvidarnos de las carencias que tengan estos navegadores antiguos.

**Ejemplo:** Si se quisiera usar **border-radius**, se debe tener en cuenta que es una función de **CSS3**, la cual no la soporta los navegadores de versiones antiguas, por lo tanto se debe hacer uso de los Polyfill, como se muestra a continuación:

Se tiene un archivo html en donde instanciamos un div de clase “cajita”,así:

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/1.png)](Imagen1)

También un archivo css donde, creamos un atributo **border-radius** dependiendo de los navegadores, agregamos algunos atributos básicos y finalmente llamamos una función llamada “behavior” que se encuentra en el archivo “PIE.htc” que lo podemos descargar en (http://css3pie.com/), que será la encargada de enseñarle a los navegadores que no soportan directamente **border-radius** a ejecutar este atributo.

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/2.png)](Imagen2)

Así se puede ver en Google Chrome:

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/3.png)](Imagen3)

Y así en Internet Explorer 8:

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/4.png)](Imagen4)

Por lo cual podemos decir que Polyfill es una buena opción a la hora utilizar nuevas técnicas de CSS3.

# Términos 

**Transpilador:** Un transpilador (también llamado compilador de origen a fuente o un transcompilador) es un programa que traduce un código fuente de un lenguaje a otro en el mismo nivel de abstracción (lo que lo hace diferente de un compilador cuya salida es de nivel inferior a su entrada ). El transcompilador puede utilizar como lenguaje de entrada un existente como Coffeescript que se transcompila a Javascript. El idioma de entrada puede ser un superconjunto del lenguaje de salida, lo que significa que cualquier código escrito en el lenguaje de salida es válido para el idioma de entrada.

**Compilador:** Es el encargado de queeEl código fuente sea transformado a lenguaje de máquina , y empaquetado en un archivo ejecutable previo a su ejecución. El programa final solo funcionara en la arquitectura para la cual fue compilado.

**Interprete:** El código fuente es leído y transformado a lenguaje de máquina por un intérprete, a medida que se leen los archivos a ejecutar en secuencia. Los errores de compilación (sintaxis, verificaciones lógicas) pueden aparecer durante la ejecución, en el punto que interpreta un archivo en particular. En cada ejecución, la interpretación se vuelve a realizar para todos los archivos utilizados. Ejemplos son el interprete de PHP y Perl.

**Preprocesador:** Un preprocesador web es una herramienta de software que modifica los datos para ajustarse a los requisitos de entrada de otro programa. 
En otras palabras, antes de ( pre ) usarlo en el programa de destino, transforma ( procesa) algunos datos, para que el resultado final pueda ser entendido / utilizado en otro programa. 
El principal beneficio de un preprocesador es que normalmente permite escribir menos código, de una manera más legible por el ser humano. Esto permite la simplificación del código complejo, junto con una mayor facilidad de mantenimiento.

Transpilador |                  Ventajas                | Desventajas
------------ | -----------------------------------------| ------------
Babel        |  Herramienta que nos permite transformar nuestro código JS de última generación (o con funcionalidades extras) a JS que cualquier navegador o versión de Node.js entienda. | 
Babel      |Cuando usamos Babel hay muchos plugins que podemos usar que no son parte de ningún preset y que nos dan ciertas mejoras de rendimiento, minificación u otras funciones que nos pueden ser útiles|
TypeScript |     Altamente compatible con Javascript.  |Requiere un proceso de precompilación antes de poder usarse
TypeScript |Herramientas para el uso de propiedades de programación orientada a objetos.  |
TypeScript |     Más fácil de usar con Angular. |


Compilador   | Ventajas     | Desventajas
------------ | ------------ | ------------
Prepos       | Fácilmente podemos compilar idiomas de preprocesamiento como sass, less , babel, entre otros. | 
Prepos     |Se sincroniza con el navegador.|


Preprocesador   | Ventajas     | Desventajas
------------    | ------------ | ------------
CoffeeScript    |Intento de exponer las partes buenas de JavaScript de una manera sencilla.
CoffeScript   |No hay diferencia entre declarar una variable y asignarle un valor, lo que tiende al scope.
Sass     |Hace más modular y organizado el código.|
Sass     |Reducción de costos.|
Sass     |Soporte ágil y rápido.|
Stylus   |Es eficiente, dinámico y expresivo para crear archivos css.|
Stylus   |Uso de variables, mixins, funciones y condicionales.|
Stylus   |Reutilización de código.|

**-** Ambos transpiladores tienen características que los hacen ser buenos, escoger  entre cual usar por ser mejor, iría en los gustos y las necesidades de los desarrolladores.
**-** Prepos es una buena opción de compilador a la hora de ahorrar tiempo.
**-** CoffeScrript es uno de los mejores preprocesadores por la facilidad que tienen para hacer el código más fácil y entendible. 
**-** Entre Sass y Less, para decidir cuál es el mejor, se debe tener en cuenta la necesidad del desarrollador, ya que el uso de Sass y Less depende de que otras tecnologías se usan, para que estos preprocesadores se manejen más fácil.

# Bundler
Sirve como manejador de dependencias para Ruby. Se puede usar para manejar las dependencias de cualquier proyecto de ruby. Proporciona un entorno seguro para los proyectos de ruby, en el cual le hace un seguimiento  a sus gemas ( librerías) y verifica que  estén en las versiones que se necesitan.

Como Bundler funciona también como una gema lo primero que hay que hacer es instalarla.

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/5.png)](Imagen5)

Bundler nos permite manejar todas las gemas de nuestro proyecto de tal modo que las coloca todas en un archivo de texto llamado  `Gemfile`  en el que vamos a listar todas las gemas (librerías) de las que dependerá nuestro proyecto.

[![N|Solid](https://bytebucket.org/katherinevera96/parcial-web/raw/950d5941d47fdf05886b58c258d89474ad8ac09a/img/6.png)](Imagen6)

La línea en la que se encuentra el Source  la URL es el servidor donde se deben instalar las gemas.
Las tres líneas de código restante son las librerías que queremos instalar en nuestro proyecto seguida cada una de su versión a instalar. Si no se le especifica que versión quieres utilizar Bundler lo tomara como que deseas utilizar la versión más reciente.

Bundlers   | Ventajas     | Desventajas
------------ | ------------ | ------------
WebPack| Permite automatizar tareas repetitivas.|Necesita un archivo de configuración para ejecutarse.
WebPack| Permite transpilar y procesar código de .sccs a .css,  también permite transpilar código de ES6 a ES5.|Ofrece la misma funcionalidad para todo tipo de proyecto, en el caso de ser un proyecto simple puede ser que no necesitemos todas las funcionalidades que ofrece Webpack.
WebPack| Permite trabajar con cualquier tipo de archivo(preprocesadores de CSS, JavaScript).|La curva de aprendizaje es elevada, necesita mucha configuración y es compleja al principio.
WebPack|Permite crear un servidor web de desarrollo. |
WebPack|Modularizar las diferentes secciones de nuestro proyecto. |
WebPack|Permite recargar la página automáticamente cuando nota cambios en los archivos Javascript del proyecto gracias  a la opción de loader. |
Browserify| Permite gestionar dependencias en forma de módulos del lado del cliente.| Browserify depende de Gulp o Grunt para realizar la mayoría de las tareas.
Browserify|Permite configurarse por medio de un script |
Browserify|Browserify tiende a ser más fácil de manejarse y su ecosistema npm es muy práctico para proyectos pequeños. |

## ¿Qué tienen en común npm, bower & composer? ##

**NPM:** Es el gestor de paquetes de Node Js. Por lo tanto gracias a él tenemos la facilidad de ejecutar cualquier librería disponible para Node Js en tan solo una línea de comando. Nos ayuda a administrar nuestros módulos, distribuir paquetes y agregar dependencias de una manera sencilla.

**Composer:** Composer es un gestor de dependencias en proyectos para programación en PHP, nos permite gestionar distintas acciones como descargar, declarar o mantener actualizado los paquetes que forman la base de nuestro proyecto PHP.
Composer nos propone algo muy interesante y  que los todos los paquetes que queremos incluir en nuestro proyecto los escribimos en un archivo de configuración, este archivo será un JSON.

**Bower:**  Tiene como función tener actualizadas las dependencias de un proyecto web, cuenta con una sencilla API de comandos útiles que realizan tareas de mantenimiento y administración de los paquetes necesarios para construir nuestro proyecto web. Con Bower podemos descargar y actualizar todo tipo de librerías, frameworks, plugins, etc.

Los tres anteriores gestores de paquetes y dependencias tienen en  común que se necesitan de estos tres gestores para crear un proyecto orientado en la web en entornos nodejs y que necesitan  mantener sus  librerías y frameworks  actualizados.
