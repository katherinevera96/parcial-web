var path = require('path');
var webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports={
    entry: path.join(__dirname,'./src/prueba.js'),
    output: {
        path: path.join(__dirname,'./libs'),
        filename: './src/js/bundle.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loader:['babel-loader'],
            exclude: /node_modules/
        },
        /*{
             test: /\.scss$/,
            loader:'style-loader!css-loader!sass-loader',
            exclude: /node_modules/
        },*/
        {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use:[ 'css-loader', 'sass-loader']
            })
      }
        ]
    },     
   
    devServer:{
        host: 'localhost',
        port: 8080,
        inline: true

    },
    plugins:[
        new webpack.optimize.UglifyJsPlugin({
            compress: { 
                warnings: false
            }
        }),
        new ExtractTextPlugin("./src/css/style.css"),
    ]
};