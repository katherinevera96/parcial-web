# Taller Parcial Elect. Desarrollo Web 2017-2
##### Profesor: Pablo Bejarano
##### Fecha de entrega: 4 de Septiembre de 2017
##### El taller preparcial equivale al 10% del corte
##### El examen parcial equivale al 10% del corte
##### El ejercicio de F1 + La investigación independiente y los demás compromisos equivalen al 10% del corte
#
#
El taller será desarrollado y enregado en parejas mediante la adición del docente al repositorio con permisos de Administración. 

El taller busca reforzar sus competencias teóricas y prácticas en desarrollo web, cubriendo temas de programación orientada a objetos, diseño web, desarrollo de UX, ES6, Bundlers, Preprocesadores, transpiladores.

## Investigación & solución de problemas (Parte Teórica) 50%

Todas sus respuestas a esta parte deben ir diligenciadas en el archivo PT.md, para poder diligenciarlo le recomiendo ver un poco la [sintaxis de los arhcivos md](https://guides.github.com/features/mastering-markdown/) y ayudarse con el [visualizador en línea](http://dillinger.io/).

1. La empresa **Arropados Ltda.** Cuenta con un sitio web para la exposición y venta de sus productos, desafortunadamente el sitio se encuentra desarrollado con la tecnología Flash. **Arropados Ltda.** Requiere que el sitio sea soportado por dispositivos móviles y no manifiestan inconvenientes con la inversión a realizar. Siendo usted todo un experto en desarrollo frontend y teniendo en cuenta que el sitio es sólo de exhibición (no cuenta con bases de datos relacionales, quizá contenidos cargados desde un JSON), plantee la mejor solución para su cliente seleccionando:
    + Tipo de diseño
    + Web Layout
    + Tecnologías

2. **Surrender20 Co.** (una empresa de asesoría en línea) cuenta con un sitio **fluido** y requiere que éste sea soportado no sólo por computadoras de escritorio (como lo hace hasta ahora), sino que también desea llegar a clientes con tabletas, siendo esta una versión con menos accesos para mayor experiencia de usuario y acceso a la información. El presupuesto para el proyecto no es muy elevado.

Escriba su estrategia para **Surrender20 Co.** argumentando las decisiones a tomar.

3. ¿Qué es Polyfill? Desarolle un ejemplo de Polyfill.

4. ¿En qué consisten lo siguientes términos?
    + Transpilar
    + Compilar
    + Interpretar
    + Preprocesar

5. Cree una tabla comparativa para cada uno de los términos de la pregunte anterior en donde se llenen los siguientes datos:

Transpilador | Ventajas     | Desventajas
------------ | ------------ | ------------
Foo          | lorem ipsum  | Dolor Amet

Compilador   | Ventajas     | Desventajas
------------ | ------------ | ------------
Foo          | lorem ipsum  | Dolor Amet

Y seleccione un ganador por cada una de las categorías. **Nota:** Los preprocesadores deben ser comparados por aparte para CSS y para JS.

6. ¿Qué es un bundler y para qué se utilizan?

7. Cree una tabla comparativa de bundlers siguiendo los mismos lineamientos del punto 5.

8. ¿Qué tienen en común npm, bower y composer?

## Desarrollo (Parte Práctica) 50%

### La idea - ¿Qué vamos a hacer?

Vamos a desarrollar un sitio web interactivo para la creación de música. Ustedes tienen dos opciones, los amantes de lo tradicional podrán desarrollar un [Piano en línea](https://virtualpiano.net/) y los amantes de la nueva escuela desarrollarán un [Launchpad en línea](https://agile-spire-1086.herokuapp.com/).

### Base

Dentro de éste repositorio se encuentra una carpeta llamada Base, con la inicialización de un proyecto, recuerde editar los archivos de configuración para poner los autores y repositorio. Los proyectos con malos archivos de configuración no serán calificados.

## Funcionalidades del sistema

Las funcionalidades:

1. **Bronce** son aquellas que deben estár en el producto final para poder ser viable, si alguna funcionalidad bronce no está, el producto no es calificable.
2. **Plata** son funcionalidades que suman puntos al proyecto y por las cuales usted podra sumer hasta 5 puntos en la parte teórica.
3. **Oro** son funcionalidades bonus que usted podrá hacer de manera opcional, lo que significa que no hacerlas no hará que su parte práctica tenga una nota inferior, pero realizarlas otorgará bonificaciones a la calificación ganada por puntos plata.

**Nota:** Las funcionalidades valen 0, 0.5 ó 1, es decir, nada, la mitad o toda.

### Piano Online

#### Bronce

1. El piano contará con al menos una escala (Recuerdo su curso de Audio, oh no, usted no vio uno? es hora de ir a la batiwikipedia).
2. El piano puede contener sólo las teclas, no hace falta que contenga accesorios ni tableros, pero el diseño debe funcionar perfectamente desde un iPad hasta un monitor full HD.
3. Usted usará ES6 para el desarrollo del proyecto (concatenaciones, funciones, clases, constantes, block scoped variables, etc). [ver más](http://es6-features.org/#Constants)

### Plata

1. Las teclas podrán ser tocadas con el mouse mediante click o con el teclado mediante las teclas (:V).
2. su proyecto es OOP, por ende contempla clases para cada elemento del sistema como lo son:
    + Instrument
    + Piano
    + Key
    + Sound
Cada elemento tendra un DOMElement como atributo que será renderizado en la aplicación para que el usuario interactue.
3. Cada interacción con el instrumento se verá reflejada en la interfaz con una acción, como por ejemplo, al oprimir una tecla (por el mouse con click o por el teclado) se sombreará la misma y se reproducirá el sonido.
4. El piano tiene un sistema de partituras en el que podremos escribir nuestras composiciones para que sean reproducidas de manera secuencial. La partitura se escribe con las teclas marcadas para cada tecla del instrumento (como en el ejemplo del piano en línea). 
5. Dentro de la partitura existen caracteres especiales:
    + | : usado para denotar un silencio
    + [] : usado para hacer corcheas, es decir, todas las notas dentro de ellos suenan a la mitad del tiempo

### Oro

1. El usuario podrá grabar partituras mediante el boton "grabar", el cual irá escribiendo la secuencia de notas tocadas en el área de partituras. (+0.5)
2. El piano contará con partituras precompuestas almacenadas en un objeto JSON dentro del código y estas partituras contemplan: (+0.5)
    + La marcha imperial
    + Epona's Song
    + Games of Thrones Intro
3. Al dejar presionada una tecla el sonido se mantendrá constante, hasta que se suelte. (+1)

### Launchpad Online

#### Bronce

1. El launchpad contará con al menos una matriz de botónes de 4x10.
2. El launchpad puede contener sólo los botones, no hace falta que contenga accesorios ni tableros, pero el diseño debe funcionar perfectamente desde un iPad hasta un monitor full HD.
3. Usted usará ES6 para el desarrollo del proyecto (concatenaciones, funciones, clases, constantes, block scoped variables, etc). [ver más](http://es6-features.org/#Constants)

### Plata

1. Las teclas podrán ser sólo con el teclado mediante las teclas (:V). 
2. Si usted toca una tecla mientras presiona Ctrl, significa que dejará presionado ese boton del launchpad hasta tocarlo nuevamente.
3. su proyecto es OOP, por ende contempla clases para cada elemento del sistema como lo son:
    + Instrument
    + Launchpad
    + Button
    + Sound
Cada elemento tendra un DOMElement como atributo que será renderizado en la aplicación para que el usuario interactue.
3. Cada interacción con el instrumento se verá reflejada en la interfaz con una acción, como por ejemplo, al oprimir una tecla se sombreará el botón asociado a la misma y se reproducirá el sonido.
4. El Launcpad tiene un sistema de partituras en el que podremos escribir nuestras composiciones para que sean reproducidas de manera secuencial. La partitura se escribe con las teclas marcadas para cada botón del instrumento (como en el ejemplo del launchpad en línea). 
5. Dentro de la partitura existen caracteres especiales:
    + | : usado para denotar un silencio
    + [] : usado para hacer corcheas, es decir, todas las notas dentro de ellos suenan a la mitad del tiempo

### Oro

1. El usuario podrá grabar partituras mediante el boton "grabar", el cual irá escribiendo la secuencia de notas tocadas en el área de partituras. (+0.5)
2. El Launchapd contará con al menos una partitura precompuesta almacenadas en un objeto JSON dentro del código haciendo un dubstep que tenga al menos 20s de duración con un buen drop al menos 5 segundos antes de terminar. Inspiración en [éste video](https://www.youtube.com/watch?v=LQU9RFTR_84) (+0.5)
3. Al dejar presionada una tecla el sonido se mantendrá constante, hasta que se suelte. (+1)

## Entrega

El proyecto debe ser subido a un repositorio en Bitbucket, para esto recuerdo borrar el .git que ya posee el proyecto para poder subirlo como un nuevo proyecto. Todos los proyectos serán revisados en el commit anterior a la hora de clase, es decir, sólo se calificarán proyectos subidos antes de las 2:30 pm del 4 de septiembre de 2017.

Muchos exitos.

# Recuerde

Para evitar pesos adicionales al proyecto el archivo base/.gitignore evitó que se subieran los "node_modules" recuerde entonces que usted debería usar el comando

`npm install`

para decargarlos y poder trabajar.