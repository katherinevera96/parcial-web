export class Key{

    constructor(nombre,letra,src){
        this._nombre = nombre;
        this._letra  = letra;
        this._src   = src;
        this._DOMElement = null;
        this.initDOMElement();
    }

    get nombre(){
        return this._nombre;
    }

    set nombre(val){
        this._nombre = val;
    }

    set letra(val){
        this._letra = val;
    }

    get letra(){
        return this._letra;
    }

    set src(val){
        this._src = val;
    }

    get src(){
        return this._src;
    }

    get DOMElement(){
        return this._DOMElement;
    }

    initDOMElement(){
        let el = document.createElement("div");
        this._DOMElement = el;
    }

}