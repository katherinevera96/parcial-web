export class Sound{

    constructor(nota,src){
        this._nota = nota;
        this._src = src;        
        this._DOMElement = null;
        this.initDOMElement();
    }

    get nota(){
        return this._a;
    }

    set nota(val){
        this._nota = val;
    }

    get src(){
        return this._src;
    } 

    set sonido(val){
        this._sonido = val;
    }

    get DOMElement(){
        return this._DOMElement;
    }

    initDOMElement(){
        let el = document.createElement("div");
        this._DOMElement = el;
    }

}