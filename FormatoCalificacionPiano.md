# Taller Parcial Elect. Desarrollo Web 2017-2
## Hoja de retroalimentación y calificación

### Estudiantes:

> Daniel Martinez Cod: 1145889
 
> Estudiante 2 Cod: xxxxx

#
#
Recodemos que...

```
El taller será desarrollado y enregado en parejas mediante la adición del docente al repositorio con permisos de Administración. 

El taller busca reforzar sus competencias teóricas y prácticas en desarrollo web, cubriendo temas de programación orientada a objetos, diseño web, desarrollo de UX, ES6, Bundlers, Preprocesadores, transpiladores.
```
## Investigación & solución de problemas (Parte Teórica) 50%


1. La empresa **Arropados Ltda.** Cuenta con un sitio web para la exposición y venta de sus productos, desafortunadamente el sitio se encuentra desarrollado con la tecnología Flash. **Arropados Ltda.** Requiere que el sitio sea soportado por dispositivos móviles y no manifiestan inconvenientes con la inversión a realizar. Siendo usted todo un experto en desarrollo frontend y teniendo en cuenta que el sitio es sólo de exhibición (no cuenta con bases de datos relacionales, quizá contenidos cargados desde un JSON), plantee la mejor solución para su cliente seleccionando:
    + Tipo de diseño
    + Web Layout
    + Tecnologías
	
	**Valor obtenido (0 - 1):** 1
	
	***

2. **Surrender20 Co.** (una empresa de asesoría en línea) cuenta con un sitio **fluido** y requiere que éste sea soportado no sólo por computadoras de escritorio (como lo hace hasta ahora), sino que también desea llegar a clientes con tabletas, siendo esta una versión con menos accesos para mayor experiencia de usuario y acceso a la información. El presupuesto para el proyecto no es muy elevado.

	Escriba su estrategia para **Surrender20 Co.** argumentando las decisiones a tomar.

	**Valor obtenido (0 - 1):** 1
	
	***

3. ¿Qué es Polyfill? Desarolle un ejemplo de Polyfill.

	**Valor obtenido (0 - 1):** 1
	
	***
	
4. ¿En qué consisten lo siguientes términos?
    + Transpilar
    + Compilar
    + Interpretar
    + Preprocesar

	**Valor obtenido (0 - 1):** 1
	
	***
	
5. Cree una tabla comparativa para cada uno de los términos de la pregunte anterior en donde se llenen los siguientes datos:

	Transpilador | Ventajas     | Desventajas
	------------ | ------------ | ------------
	Foo          | lorem ipsum  | Dolor Amet
	
	Compilador   | Ventajas     | Desventajas
	------------ | ------------ | ------------
	Foo          | lorem ipsum  | Dolor Amet

	Y seleccione un ganador por cada una de las categorías. **Nota:** Los preprocesadores deben ser comparados por aparte para CSS y para JS.
	
	**Valor obtenido (0 - 1):** 1
	
	***

6. ¿Qué es un bundler y para qué se utilizan?

	**Valor obtenido (0 - 1):** 0.5
	
	***

7. Cree una tabla comparativa de bundlers siguiendo los mismos lineamientos del punto 5.

	**Valor obtenido (0 - 1):** 1
	
	***

8. ¿Qué tienen en común npm, bower y composer?

	**Valor obtenido (0 - 1):** 1
	
	***
	
### Puntuación Parte Teórica ( 7.5 / 8 ): 4.7
	
***
	
## Desarrollo (Parte Práctica) 50%

### Funcionalidades del sistema

Recordemos que:
```
Nota: Las funcionalidades valen 0, 0.5 ó 1, es decir, nada, la mitad o toda.
```
### Piano Online

#### Bronce

1. El piano contará con al menos una escala (Recuerdo su curso de Audio, oh no, usted no vio uno? es hora de ir a la batiwikipedia).

	**Alcanzado (Si - No):** si
	
	***
2. El piano puede contener sólo las teclas, no hace falta que contenga accesorios ni tableros, pero el diseño debe funcionar perfectamente desde un iPad hasta un monitor full HD.

	**Alcanzado (Si - No):** Si
	
	***
3. Usted usará ES6 para el desarrollo del proyecto (concatenaciones, funciones, clases, constantes, block scoped variables, etc). [ver más](http://es6-features.org/#Constants)

	**Alcanzado (Si - No):** si
	
	***

### Plata

1. Las teclas podrán ser tocadas con el mouse mediante click o con el teclado mediante las teclas (:V).

	**Valor obtenido (0 - 1):** 1
	
	***
	
2. su proyecto es OOP, por ende contempla clases para cada elemento del sistema como lo son:
    + Instrument
    + Piano
    + Key
    + Sound
Cada elemento tendra un DOMElement como atributo que será renderizado en la aplicación para que el usuario interactue.

	**Valor obtenido (0 - 1):** 0.5
	
	***
	
3. Cada interacción con el instrumento se verá reflejada en la interfaz con una acción, como por ejemplo, al oprimir una tecla (por el mouse con click o por el teclado) se sombreará la misma y se reproducirá el sonido.

	**Valor obtenido (0 - 1):** 1
	
	***
4. El piano tiene un sistema de partituras en el que podremos escribir nuestras composiciones para que sean reproducidas de manera secuencial. La partitura se escribe con las teclas marcadas para cada tecla del instrumento (como en el ejemplo del piano en línea). 

	**Valor obtenido (0 - 1):** 0.2
	
	***
	
5. Dentro de la partitura existen caracteres especiales:
    + | : usado para denotar un silencio
    + [ ] : usado para hacer corcheas, es decir, todas las notas dentro de ellos suenan a la mitad del tiempo
	
	**Valor obtenido (0 - 1):** 0
	
	***
	
### Oro

1. El usuario podrá grabar partituras mediante el boton "grabar", el cual irá escribiendo la secuencia de notas tocadas en el área de partituras. (+0.5)

	**Alcanzado (Si - No):**
	
	***
	
2. El piano contará con partituras precompuestas almacenadas en un objeto JSON dentro del código y estas partituras contemplan: (+0.5)
    + La marcha imperial
    + Epona's Song
    + Games of Thrones Intro

	**Alcanzado (Si - No):**
	
	***
	
3. Al dejar presionada una tecla el sonido se mantendrá constante, hasta que se suelte. (+1)

	**Alcanzado (Si - No):**
	
	***

### Puntuación Parte Práctica: 2.7
### Bonificaciones por oro:

## Nota Final: (4.7 + 2.7) / 2 : 3.7